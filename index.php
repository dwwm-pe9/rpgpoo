
    <?php
    require_once('vendor/autoload.php');

use Src\Entity\Barbarian;
use Src\Entity\Player;
    use Src\Entity\Role;
    use Src\Entity\Skill;
    use Src\Entity\RoleStat;
    use Src\Entity\Weapon;
    use Src\Entity\Wizard;

    //Weapons 3
    $sword = new Weapon("Épée", "Excalibur du bled", 50);
    $hammer = new Weapon("Masse", "Attention les doigts", 100);
    $staff = new Weapon("Coiffe de Rabadon", "Chapeau rouge", 75);
    //Skill 3
    $fireBall = new Skill("Boule de feu", 75, 25);
    $heroicStrike = new Skill("Frappe héroique", 50, 30);
    $knifeThrow = new Skill("Lancé de couteau", 75, 25);

    //RoleStat 1 Magicien 1 Barbare

    //création de sstats pour le wizard et barbare " a la main "
    // $wizardStat = new RoleStat(80, 40, 50, 60, 100);
    // $barbarianStat = new RoleStat(70, 60, 30, 100, 60);

    //Utilisation d'une methode statique qui recupere les constantes de la classe
    $wizardStat = RoleStat::getWizardStat();
    $barbarianStat =  RoleStat::getBarbarianStat();

    //Création d'un Magicien en paramettrant le nom et les stats
    $wizard1 = new Role("Wizard", $wizardStat);
    //Création d'un Magicien avec les valeurs par defaut de la classe Wizard
    $wizard2 = new Wizard();

    $wizard1->addSkills($fireBall);
    $wizard2->addSkills($fireBall);
    $wizard1->addSkills($knifeThrow);
    $wizard2->addSkills($knifeThrow);

    $wizard1->addWeapons($staff);
    $wizard2->addWeapons($staff);


    // Création Barbare 
    //$barbarian = new Role("Barbarian", $barbarianStat);

    $barbarian = new Barbarian();
    $barbarian->addSkills($heroicStrike);
    $barbarian->addSkills($knifeThrow);

    $barbarian->addWeapons($sword);

    // Player
    $player = new Player("Gaëtan", $wizard2);

    $ennemy = new Player("BOT Gandalf", $wizard1, 50);

    dump($player, $ennemy);

    $saruman = new Wizard();
    ?>
