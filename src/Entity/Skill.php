<?php
namespace Src\Entity;

class Skill
{

    private string $title;
    private float $damage;
    private float $cost;
   
    //Cette methode magique est appellé lorsque j'instancie un objet avec NEW
    public function __construct(string $title, float $damage, float $cost)
    {
        $this->title = $title;
        $this->damage = $damage;
        $this->cost = $cost;
    }

    /**
     * Get the value of title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of damage
     */
    public function getDamage()
    {
        return $this->damage;
    }

    /**
     * Set the value of damage
     *
     * @return  self
     */
    public function setDamage($damage)
    {
        $this->damage = $damage;

        return $this;
    }

    /**
     * Get the value of cost
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set the value of cost
     *
     * @return  self
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }
}
