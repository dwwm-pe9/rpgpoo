<?php
namespace Src\Entity;

class Role{

    protected string $name;
    protected RoleStat $stats;
    protected array $skills;
    protected array $weapons;

    public function __construct(string $name, RoleStat $stats, array $skills = [], array $weapons = [])
    {
        $this->name = $name;
        $this->stats = $stats;
        $this->weapons = $weapons;
        $this->skills = $skills;
    }
    /**
     * Get the value of stats
     */ 
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * Set the value of stats
     *
     * @return  self
     */ 
    public function setStats($stats)
    {
        $this->stats = $stats;

        return $this;
    }

    /**
     * Get the value of skills
     */ 
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set the value of skills
     *
     * @return  self
     */ 
    public function addSkills(Skill $skill)
    {
        $this->skills[] = $skill;

        return $this;
    }

    public function removeSkill(Skill $skill)
    {   
        // Verifiér si le sskill est dans les SKILLS de mon objet
        if(in_array($skill,$this->skills)){
            //Je cherche son indice
            $i = array_search($skill,$this->skills);
            //J'unset l'indice trouvé
            unset($this->skills[$i]);
        }        
    }

    /**
     * Get the value of weapons
     */ 
    public function getWeapons()
    {
        return $this->weapons;
    }

    /**
     * Set the value of weapons
     *
     * @return  self
     */ 
    public function addWeapons(Weapon $weapon)
    {
        $this->weapons[] = $weapon;

        return $this;
    }

    public function removeWeapon(Weapon $weapon)
    {  
        if(in_array($weapon,$this->weapons)){
            $i = array_search($weapon,$this->weapons);
            unset($this->weapons[$i]);
        }        
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}


