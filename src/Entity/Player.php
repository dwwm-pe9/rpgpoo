<?php
namespace Src\Entity;

use Src\Entity\Role;

class Player{

    private string $name;
    private Role $role;
    private int $level;
    private int $exp = 0;

    public function __construct(string $name, Role $role, int $level = 1)
    {
        $this->name = $name;
        $this->role = $role;
        $this->level = $level;
    }
    
    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of role
     */ 
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @return  self
     */ 
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get the value of level
     */ 
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set the value of level
     *
     * @return  self
     */ 
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get the value of exp
     */ 
    public function getExp()
    {
        return $this->exp;
    }

    /**
     * Set the value of exp
     *
     * @return  self
     */ 
    public function setExp($exp)
    {
        $this->exp = $exp;

        return $this;
    }
}


