<?php


namespace Src\Entity;

class RoleStat{
    
    private float $atk;
    private float $def;
    private float $speed;
    private float $hp;
    private float $mana;

    public const WIZARD_ATK = 60;
    public const WIZARD_DEF = 40;
    public const WIZARD_SPEED = 40;
    public const WIZARD_HP = 60;
    public const WIZARD_MANA = 100;

    public const BARBARIAN_ATK = 80;
    public const BARBARIAN_DEF = 50;
    public const BARBARIAN_SPEED = 35;
    public const BARBARIAN_HP = 60;
    public const BARBARIAN_MANA = 80;

    public const ROGUE_ATK = 50;
    public const ROGUE_DEF = 30;
    public const ROGUE_SPEED = 65;
    public const ROGUE_HP =  40;
    public const ROGUE_MANA = 80;

    public function __construct(float $atk, float $def, float $speed,float $hp, float $mana)
    {
        $this->atk = $atk;
        $this->def = $def;
        $this->speed = $speed;
        $this->hp = $hp;
        $this->mana = $mana;
    }
    
    static function getWizardStat(){
        return new self(self::WIZARD_ATK,self::WIZARD_DEF,self::WIZARD_SPEED,self::WIZARD_HP,self::WIZARD_MANA);
    }

    static function getBarbarianStat(){
        return new self(self::BARBARIAN_ATK,self::BARBARIAN_DEF,self::BARBARIAN_SPEED,self::BARBARIAN_HP,self::BARBARIAN_MANA);
    }

    static function getRogueStat(){
        return new self(self::ROGUE_ATK,self::ROGUE_DEF,self::ROGUE_SPEED,self::ROGUE_HP,self::ROGUE_MANA);
    }

    /**
     * Get the value of atk
     */ 
    public function getAtk()
    {
        return $this->atk;
    }

    /**
     * Set the value of atk
     *
     * @return  self
     */ 
    public function setAtk($atk)
    {
        $this->atk = $atk;

        return $this;
    }

    /**
     * Get the value of def
     */ 
    public function getDef()
    {
        return $this->def;
    }

    /**
     * Set the value of def
     *
     * @return  self
     */ 
    public function setDef($def)
    {
        $this->def = $def;

        return $this;
    }

    /**
     * Get the value of speed
     */ 
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set the value of speed
     *
     * @return  self
     */ 
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get the value of hp
     */ 
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * Set the value of hp
     *
     * @return  self
     */ 
    public function setHp($hp)
    {
        $this->hp = $hp;

        return $this;
    }

    /**
     * Get the value of mana
     */ 
    public function getMana()
    {
        return $this->mana;
    }

    /**
     * Set the value of mana
     *
     * @return  self
     */ 
    public function setMana($mana)
    {
        $this->mana = $mana;

        return $this;
    }
}


